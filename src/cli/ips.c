/* Copyright (C) 2016-2022  Alejandro Sanchez
 *
 * IPS-Tools is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * IPS-Tools is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * IPS-Tools.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <string.h>
#include <stdarg.h>
#include <stdbool.h>
#include <ipstools.h>

// The real value will be defined at compile-time by the compiler
#ifndef IPS_TOOLS_VERSION
#define IPS_TOOLS_VERSION "0.0.0"
#endif /* ifndef IPS_VERSION */


void print_usage(FILE *f);

/** Whether the given string equals one of the other strings. */
bool str_one_of(const char *this, int n, ...) {
	va_list ap;
	va_start(ap, n);
	for (int i = 0; i < n; ++i) {
		char *other = va_arg(ap, char *);
		if (strcmp(this, other) == 0) {
			return true;
		}
	}
	va_end(ap);
	return false;
}

int info();
int patch(char const *restrict patch_fname);
int diff(char const *restrict original_fname, char const *restrict modified_fname);


int main(int argc, char *argv[]) {
	if (argc < 2) {
		print_usage(stderr);
		return 1;
	}

	int result = 0;
	const char *command = argv[1];
	if (argc == 2 && str_one_of(command, 3, "help", "-h", "--help")) {
		print_usage(stdout);
	} else if (argc == 2 && str_one_of(command, 3, "version", "-v", "--version")) {
		puts(IPS_TOOLS_VERSION);
	} else if (argc == 2 && str_one_of(command, 1, "info")) {
		result = info();
	} else if (argc == 3 && str_one_of(command, 1, "patch")) {
		result = patch(argv[2]);
	} else if (argc == 4 && str_one_of(command, 1, "diff")) {
		result = diff(argv[2], argv[3]);
	} else {
		result = 1;
		print_usage(stderr);
	}

	return result;
}

int info() {
	int error = ips_info(stdin, stdout);
	switch (error) {
		case IPS_INFO_INV_FILE:
			fputs("Error: invalid patch file.", stderr);
			break;
		case IPS_INFO_INV_HEADER:
			fputs("Error: invalid patch file header.", stderr);
			break;
		case IPS_INFO_INV_RECORD:
			fputs("Error: invalid patch record.", stderr);
			break;
	}
	return error ? 1 : 0;
}

int patch(char const *restrict patch_fname) {
	FILE *patch_file = fopen(patch_fname, "rb");
	if (patch_file == NULL) {
		fprintf(stderr, "Cannot open patch file '%s'\n", patch_fname);
		return 1;
	}
	int error = ips_patch(stdin, stdout, patch_file);
	fclose(patch_file);
	switch (error) {
		case IPS_PATCH_INV_FILE:
			fputs("IPS Patch: invalid file", stderr);
			break;
		case IPS_PATCH_INV_PATCH:
			fputs("IPS Patch: invalid patch file", stderr);
	}
	return error ? 1 : 0;
}

int diff(char const *restrict original_fname, char const *restrict modified_fname) {
	FILE *original = fopen(original_fname, "rb"), *modified = fopen(modified_fname, "rb");
	if (original == NULL) {
		fprintf(stderr, "Cannot open file '%s'\n", modified_fname);
	}
	if (modified == NULL) {
		fprintf(stderr, "Cannot open file '%s'\n", original_fname);
	}
	if (modified == NULL || original == NULL) {
		return 1;
	}
	int error = ips_diff(original, modified, stdout);
	fclose(original);
	fclose(modified);
	switch (error) {
		case IPS_DIFF_INV_FILE:
			fprintf(stderr, "TODO");
			break;
		case IPS_DIFF_OFFSET_OVERFLOW:
			fputs("Error: offset overflow", stderr);
			break;
	}
	return error ? 1 : 0;
}


void print_usage(FILE *f) {
	const char *message =
		"Usage: \n"
		"  ips patch <patch>                (apply patch)\n"
		"  ips diff  <original> <modified>  (generate patch)\n"
		"  ips info                         (display patch info)\n"
		"\n"
		"  ips (version | -v | --version)   (show version info)\n"
		"  ips (help | -h | --help)         (display this message)\n"
		"\n"
		"Applying a patch:\n"
		"  The source binary is read from standard input and the target binary is\n"
		"  written to standard output. <patch-file> is the file name of a valid IPS\n"
		"  binary patch file.\n"
		"\n"
		"Generating a patch:\n"
		"  The <original> and <modified> parameters are the file names of the original\n"
		"  and modified file respectively. If one of the parameters is '-' use the\n"
		"  standard input. The resulting patch is written to the standard output.\n"
		"\n"
		"Inspecting a patch:\n"
		"  The patch is read from standard input, the info is written to standard\n"
		"  output.\n";
	fputs(message, f);
}
