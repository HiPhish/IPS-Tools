################################
Overview of the source directory
################################

The source code directory is split into a number of directories for different
tasks.

ips
   This is the core of IPS-Tools, it contains common code as well as the
   modules for the individual tasks, such as applying a patch. If you want to
   change how IPS-Tools work this is the directory you are looking for.

cli
   Command-line tools for IPS-Tools. These are just wrappers that interface
   with the CLI; their purpose is to receive input from the shell, open files
   and then call the functions from the core set. If you want to change the
   shell interface this is the place.

There is no GUI in this project, a GUI should be written as a standalone
project that use libipstools as a dependency.
