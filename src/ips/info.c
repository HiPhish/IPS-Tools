/* Copyright (C) 2016-2022  Alejandro Sanchez
 *
 * IPS-Tools is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * IPS-Tools is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * IPS-Tools.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <assert.h>
#include <string.h>
#include "ips.h"

/** Hard-coded header of every patch file. */
static const char *header = "PATCH";

int ips_info(FILE *restrict patch, FILE *restrict output) {
	struct ips_record record;
	int error = IPS_INFO_SUCCESS;

	/* Pre-conditions */
	if (!patch || !output) {
		return IPS_INFO_INV_FILE;
	}

	/* Header info */
	do {
		/* One more byte for the string-terminating NUL character */
		uint8_t raw[IPS_HEADER_BYTES + 1] = {0x00};
		if (fread(raw, sizeof *raw, sizeof raw / sizeof *raw - 1, patch) != sizeof raw / sizeof *raw - 1) {
			error = IPS_INFO_INV_FILE;
			goto end;
			
		}
		if (memcmp(raw, header, strlen(header))) {
			fprintf(output, "Invalid patch header: %s\n", raw);
			error = IPS_INFO_INV_HEADER;
			goto end;
		}
		fprintf(output, "%s\n", raw);
	} while(0);

	/* Records loop; will keep looping until encountering `EOF`. */
	while(1) {
		if (ips_read_record(patch, &record)) {
			error = IPS_INFO_INV_RECORD;
			goto end;
		} else if (record.tag == IPS_RECORD_EOF) {
			break;
		}

		char *type;
		if (record.tag == IPS_RECORD_PLAIN) {
			type = "plain";
		} else if (record.tag == IPS_RECORD_COMPRESSED) {
			type = "compressed";
		} else {
			assert(0);
		}
		fprintf(output, "0x%06lX: %5ld bytes (%s)\n", record.offset, record.length, type);
		if (record.tag == IPS_RECORD_PLAIN) { /* Prevent memory leak */
			free(record.data);
			record.data = NULL;
		}
	}

	fprintf(output, "EOF\n");

	/* Truncation */
	do {
		unsigned long truncate;
		if (feof(patch)) {
			goto end;
		}
		if (ips_read_truncate(patch, &truncate)) {
			error = IPS_INFO_INV_FILE;
			goto end;
		}
		fprintf(output, "Truncate: %06lX\n", truncate);
	} while(0);

end:
	return error;
}

