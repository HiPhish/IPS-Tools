/* Copyright (C) 2016-2022  Alejandro Sanchez
 *
 * IPS-Tools is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * IPS-Tools is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * IPS-Tools.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdlib.h>
#include <stdint.h>
#include <assert.h>
#include "ips.h"

/* Number of items in an array */
#define NITEMS(array) (sizeof array / sizeof *array)

/** Convert array of bytes into a long integer.
 *
 *  @param bp  Byte pointer.
 *  @param n   Length of byte array
 *
 *  @return  Long integer from bytes interpreted in big-endian notation.
 *
 *  The offset and size values read from the IPS patch file are unsigned, but
 *  `long` is signed. This function is still save because `long` is at least 32
 *  bits in size, whereas the offset and size are 24 and 16 bits in size
 *  respectively.
 */
static unsigned long bytes_to_ulong(uint8_t *bp, int n);

/** Convert a long integer to an array of bytes.
 *
 *  @param l   Long integer.
 *  @param bp  Array of bytes.
 *  @param n   Length of byte array.
 *
 *  The resulting array contains the three least significant bytes of the
 *  integer ordered from most to least significant.
 */
static void ulong_to_bytes(unsigned long l, uint8_t *bp, int n);

int ips_read_record(FILE *f, struct ips_record *record) {
	uint8_t raw_offset[IPS_OFFSET_BYTES];  /* Raw offset bytes */
	if (fread(raw_offset, sizeof *raw_offset, NITEMS(raw_offset), f) != NITEMS(raw_offset)) {
		return 1;
	}
	unsigned long offset = bytes_to_ulong(raw_offset, NITEMS(raw_offset));

	if (offset == IPS_END_OF_FILE) {
		record->tag = IPS_RECORD_EOF;
		record->offset = 0;
		record->length = 0;
		record->datum = 0;
		return 0;
	}

	uint8_t raw_length[IPS_LENGTH_BYTES];  /* Raw length bytes */
	if (fread(raw_length, sizeof *raw_length, NITEMS(raw_length), f) != NITEMS(raw_length)) {
		return 1;
	}
	unsigned long length = bytes_to_ulong(raw_length, NITEMS(raw_length));

	/* A non-zero length is a plain record, zero is compressed. */
	if (length) {
		uint8_t *data = NULL;
		if (!(data = malloc(length))) {
			return 1;
		} else if (fread(data, sizeof *data, length, f) != (size_t)length) {
			free(data);
			return 1;
		}
		record->tag = IPS_RECORD_PLAIN;
		record->offset = offset;
		record->length = length;
		record->data = data;
	} else {
		uint8_t datum;
		if (fread(raw_length, sizeof *raw_length, NITEMS(raw_length), f) != NITEMS(raw_length)) {
			return 1;
		} else if (fread(&datum, sizeof(uint8_t), 1, f) != 1) {
			return 1;
		}
		length = bytes_to_ulong(raw_length, NITEMS(raw_length));
		record->tag = IPS_RECORD_COMPRESSED;
		record->offset = offset;
		record->length = length;
		record->datum = datum;
	}

	return 0;
}

int ips_write_record(FILE *f, struct ips_record record) {
	uint8_t offset[IPS_OFFSET_BYTES];
	uint8_t length[IPS_LENGTH_BYTES];
	ulong_to_bytes(record.offset, offset, NITEMS(offset));
	ulong_to_bytes(record.length, length, NITEMS(length));

	if (record.tag == IPS_RECORD_EOF) {
		uint8_t bytes[] = {'E', 'O', 'F'};
		if (fwrite(bytes, sizeof *bytes, NITEMS(bytes), f) != NITEMS(bytes)) {
			return 1;
		}
		return 0;
	}
	if (fwrite(offset, sizeof *offset, NITEMS(offset), f) != NITEMS(offset)) {
		return 1;
	}

	switch (record.tag) {
		case IPS_RECORD_PLAIN:
			if (fwrite(length, sizeof *length, NITEMS(length), f) != NITEMS(length)) {
				return 1;
			} else if (fwrite(record.data, sizeof *record.data, record.length, f) != (size_t)record.length) {
				return 1;
			}
			break;
		case IPS_RECORD_COMPRESSED:
			if (fwrite((uint8_t[]){0x00, 0x00}, sizeof(uint8_t), IPS_LENGTH_BYTES, f) != IPS_LENGTH_BYTES) {
				return 1;
			} else if (fwrite(length, sizeof *length, NITEMS(length), f) != NITEMS(length)) {
				return 1;
			} else if (fwrite(&record.datum, sizeof record.datum, 1, f) != 1) {
				return 1;
			}
			break;
		default:
			return 1;
	}
	return 0;
}

int ips_read_truncate(FILE *f, unsigned long *l) {
	uint8_t raw[IPS_OFFSET_BYTES];
	if (fread(raw, sizeof *raw, NITEMS(raw), f) != NITEMS(raw)) {
		return 1;
	}
	*l = bytes_to_ulong(raw, NITEMS(raw));
	return 0;
};

int ips_write_truncate(FILE *f, unsigned long l) {
	uint8_t raw[IPS_OFFSET_BYTES];
	ulong_to_bytes(l, raw, NITEMS(raw));
	if (fwrite(raw, sizeof *raw, NITEMS(raw), f) != NITEMS(raw)) {
		return 1;
	}
	return 0;
}

static unsigned long bytes_to_ulong(uint8_t *bp, int n) {
	unsigned long l = 0;

	assert(n <= sizeof l);
	for (int i = 0; i < n; ++i) {
		l += bp[i] << 8 * (n - i - 1);
	}

	return l;
}

static void ulong_to_bytes(unsigned long l, uint8_t *bp, int n) {
	assert(n <= sizeof l);
	for (int i = 0; i < n; ++i) {
		bp[i] = l & (0xFF << (8 * (n - i - 1)));
	}
}

