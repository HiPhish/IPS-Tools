/* Copyright (C) 2016-2022  Alejandro Sanchez
 *
 * IPS-Tools is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * IPS-Tools is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * IPS-Tools.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include "ips.h"

/* Number of items in an array */
#define NITEMS(array) (sizeof array / sizeof *array)

/* Read one byte from `file` into `var` and set `eof_var` to true if the EOF
 * was hit, abort with an error otherwise. */
#define READ_BYTE(var, file, eof_var) \
	if (fread(&var, sizeof var, 1, file) != 1) { \
		if (!(eof_var = feof(file))) { \
			error = IPS_DIFF_INV_FILE; \
			goto end; \
		} \
	}


int ips_diff(FILE *restrict original, FILE *restrict modified, FILE *restrict out) {
	int error = IPS_DIFF_SUCCESS;  /* Return code */
	if (!original || !modified || !out) {  /* Pre-conditions */
		error = IPS_DIFF_INV_FILE;
		goto end;
	}

	uint8_t org_byte = 0x00, mod_byte = 0x00, prev_byte = 0x00;
	bool same, eof_org = false, eof_mod = false;

	/* Initialise the record with a maximum size data buffer, the rest will be
	 * filled in later */
	struct ips_record record = {
		.data = (uint8_t[IPS_LENGTH_MAX]){},
	};

	do {  /* Print the header */
		uint8_t bytes[] = {'P', 'A', 'T', 'C', 'H'};
		if (fwrite(bytes, sizeof *bytes, NITEMS(bytes), out) != NITEMS(bytes)) {
			error = IPS_DIFF_INV_FILE;
			goto end;
		}
	} while (0);

loop:
	do {  /* Skip bytes until we find a differing pair */
		prev_byte = org_byte;  /* Store in case we might have to back up */

		READ_BYTE(org_byte, original, eof_org)
		READ_BYTE(mod_byte, modified, eof_mod)

		if (eof_mod) {
			record.tag = IPS_RECORD_EOF;
			goto success;
		} else {
			same = eof_org ? false : org_byte == mod_byte;
		}
	} while (same);

	record.tag = IPS_RECORD_PLAIN;
	record.offset = ftell(modified) - 1;
	record.length = 0;
	record.data[record.length++] = mod_byte;

	/* If the offset is 0x454f46 it can be confused for the EOF sequence. A
	 * simple solution is to move the record one byte backwards and include the
	 * preceding byte as if it was different in the original and the modified. */
	if (record.offset == IPS_END_OF_FILE) {
		--record.offset;
		record.data[record.length++] = record.data[0];
		record.data[0] = prev_byte;
	}

	/* The IPS format cannot handle an offset too large. */
	if (record.offset > IPS_OFFSET_MAX) {
		error = IPS_DIFF_OFFSET_OVERFLOW;
		goto end;
	}
	
	do {  /* Read bytes until we find a same pair */
		READ_BYTE(org_byte, original, eof_org)
		READ_BYTE(mod_byte, modified, eof_mod)

		if (eof_mod) {
			same = true;
		} else if (eof_org || org_byte != mod_byte) {
			record.data[record.length++] = mod_byte;
		} else {
			same = true;
		}
	} while (!same && record.length < IPS_LENGTH_MAX);

	ips_write_record(out, record);
	goto loop;

success:
	ips_write_record(out, record);
	if (!eof_org) {
		/* Subtract one because the original is one byte beyond the modified. */
		unsigned long offset = ftell(modified) - 1;
		if (ips_write_truncate(out, offset)) {
			error = IPS_DIFF_INV_FILE;
		}
	}

end:
	return error;
}

