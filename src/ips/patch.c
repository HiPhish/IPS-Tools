/* Copyright (C) 2016-2022  Alejandro Sanchez
 *
 * IPS-Tools is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * IPS-Tools is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * IPS-Tools.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <assert.h>
#include "ips.h"


/** Copies a byte sequence of given length from source to target.
 *
 *  @param source  File to copy bytes from
 *  @param target  File to copy bytes to
 *  @param n       Number of bytes to copy
 *  @param off     Pointer to offset counter into target file to increase */
static int copy_bytes(FILE *restrict source, FILE *restrict target, unsigned long n, unsigned long *restrict off);

int ips_patch(FILE *restrict source, FILE *restrict target, FILE *restrict patch) {
	struct ips_record record;
	int error = IPS_PATCH_SUCCESS;
	/* We have to manually keep track of the offset because `ftell` will not
	 * work on streams, such as `stdin` and `stdout` */
	unsigned long source_offset = 0, target_offset = 0;

	/* Pre-conditions */
	if (!source || !target || !patch) {
		error = IPS_PATCH_INV_FILE;
		goto end;
	}

	/* Verify the patch header */
	do {
		uint8_t magic[IPS_HEADER_BYTES];  /* Store the magic bytes read */

		if (fread(magic, sizeof *magic, sizeof magic / sizeof *magic, patch) != sizeof magic / sizeof *magic) {
			error = IPS_PATCH_INV_PATCH;
			goto end;
		}

		/* Every IPS patch file starts with this sequence. */
		if (memcmp(magic, "PATCH", sizeof magic)) {
			error = IPS_PATCH_INV_PATCH;
			goto end;
		}
	} while(0);

read_record:
	if (ips_read_record(patch, &record)) {
		error = IPS_PATCH_INV_PATCH;
		goto end;
	}

	if (record.tag == IPS_RECORD_EOF) {
		goto eof;
	}

	/* Copy bytes before the record from source */
	do {
		long bytes_to_copy = record.offset - source_offset;
		if ((error = copy_bytes(source, target, bytes_to_copy, &target_offset))) {
			goto end;
		}
		source_offset += bytes_to_copy;
	} while (0);

	if (record.tag == IPS_RECORD_PLAIN) {
		if (fwrite(record.data, sizeof *record.data, record.length, target) != record.length) {
			error = IPS_PATCH_INV_FILE;
			goto end;
		}
		target_offset += record.length;
		free(record.data);  /* We will not longer need the data */
		record.data = NULL;
	} else if (record.tag == IPS_RECORD_COMPRESSED) {
		uint8_t buf[record.length];
		memset(buf, record.datum, record.length);
		if (fwrite(buf, sizeof *buf, sizeof buf / sizeof *buf, target) != sizeof buf / sizeof *buf) {
			error = IPS_PATCH_INV_FILE;
			goto end;
		}
		target_offset += record.length;
	} else {
		assert(0);
	}

	/* Skip over the patched bytes; ignore failure to read, this is normal if
	 * the patch extends the file */
	do {
		uint8_t junk[record.length];
		fread(junk, sizeof *junk, sizeof junk / sizeof *junk, source);
		source_offset += record.length;
	} while (0);

	goto read_record;

eof:
	do {
		/* Try reading the truncate length, if it fails there is no truncation
		 * and we wrap up the target. */
		unsigned long trunc_offset = 0;

		if (ips_read_truncate(patch, &trunc_offset)) {
			/* Copy bytes from source to target until reaching the end of
			 * source. */
			int c;
			while ((c = fgetc(source)) != EOF) {
				fputc(c, target);
			}
		} else {
			/* Truncate the target */
			long delta = trunc_offset - target_offset + 1;

			if (delta < 0) {
				error = IPS_PATCH_INV_FILE;
				goto end;
			}

			error = copy_bytes(source, target, delta, &target_offset);
		}
	} while(0);


end:
	return error;
}

static int copy_bytes(FILE *restrict source, FILE *restrict target, unsigned long n, unsigned long *restrict off) {
	for (long l = 0; l < n; ++l) {
		int c, r;  // character and return code

		c = fgetc(source);
		if (c == EOF) {
			return IPS_PATCH_INV_FILE;
		}

		r = fputc(c, target);
		if (r == EOF) {
			return IPS_PATCH_INV_FILE;
		}
	}

	*off += n;
	return IPS_PATCH_SUCCESS;
}
