/* Copyright (C) 2016-2018  Alejandro Sanchez
 *
 * IPS-Tools is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * IPS-Tools is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * IPS-Tools.  If not, see <http://www.gnu.org/licenses/>.
 */

/** @file ips.h
 *
 *  Common definitions and constants used by all of IPS-Tools.
 */

#ifndef IPS_TOOLS_PRIVATE_H
#define IPS_TOOLS_PRIVATE_H

#include <stdint.h>
#include <ipstools.h>


#define IPS_OFFSET_BYTES  3  /**< Number of bytes in a record offset entry. */
#define IPS_LENGTH_BYTES  2  /**< Number of bytes in a record length entry. */
#define IPS_HEADER_BYTES  5  /**< Number of bytes in the patch file header. */
#define IPS_ENDING_BYTES  3  /**< Number of bytes in the patch file ending. */

/** Largest possible offset of a record. */
#define IPS_OFFSET_MAX  0xFFFFFF
/** Largest possible length of a record. */
#define IPS_LENGTH_MAX  0xFFFF

/** This sequence terminates the patch file ("EOF"). */
#define IPS_END_OF_FILE  0x454F46

#endif  // IPS_TOOLS_PRIVATE_H
