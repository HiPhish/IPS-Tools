.. This document is using the reStructedText markup syntax.
.. default-role:: code

##########################################
IPS-Tools - Patching binaries the Unix way
##########################################

IPS-Tools is a suite of command-line applications and a C programming library
for patching binary files using the IPS format. I was unsatisfied with the
currently available popular patchers, so I decided to write my own. The tools
are small, simple, fast and just as powerful as some a GUI application would
be.

The design goals for IPS-Tools are:

Source code available
   I want to know what I'm executing and I want to know what the patch will do
   to my binary. Converting an image from one format to another is one thing,
   but if I'm dealing with executable code it is imperative to know how it will
   be changed.

Do one thing and do it well
   IPS-Tools provides a number of individual programs for individual tasks. You
   will never want to patch a file and generate the patch at the same time,
   therefore those two tasks do not belong in the same program.

Keep it simple
   How much time to you want to spend applying patches? Next to nothing, that's
   why the interface to IPS-Tools is as minimal as possible: no large manuals
   to study, no unnecessary options. We use the standard input and standard
   output along with redirection for all file operations.

Small
   The binaries of IPS-Tools are tiny, both on disc and when running. There is
   no recursion, no large data structures and no fear of overflowing your
   stack. This makes IPS-Tools ideal for embedding in other applications.

Portable
   IPS-Tools are written in pure standard compliant C. Building from source
   requires you only to run the portable makefile as long as you have a working
   C compiler.

Documented
   The user manual explains everything: file formats, to how to use the tools
   and the source code.


.. contents::


Building
########

The project is built using the Meson_ build system. See the INSTALL_ file for
detailed build instructions.

.. _Meson: https://mesonbuild.com/
.. _INSTALL: INSTALL.rst


How to use
##########

IPS-Tools is a collection of command-line tools and a C library. The
command-line tools use standard input and output to read and write binaries.

.. code-block:: sh

   # Patch a binary (reads the original from stdin, writes result to stdout)
   ips patch pathfile.ips <origina.bin >modified.bin

   # Print information about a patch file
   ips info <pathfile.ips

   # Generate a patch file
   ips diff origina.bin modified.bin >patchfile.ips

The C library can be used to implement these features in other applications as
well.

.. code-block:: c

   #include <ips/patch.h>

   FILE *original, *modified, *patch;
   ips_patch(original, modified, patch);

For more information see the USAGE_ file.

.. _USAGE: USAGE.rst


License
#######

Released under the GPLv3+ license, see the COPYING_ file for details.

.. _COPYING: COPYING.txt
