.. default-role:: code

####################
 Building IPS-Tools
####################

There are multiple sets of targets you can build. To build everything you need
the Meson_ build system and a suitable backend (e.g. Ninja_). Run the following
in your shell:

.. code-block:: sh

   # Replace 'build' with whatever else you want
   meson setup build
   # Build the project
   meson compile -C build
   # Install the project
   meson install -C build

The individual components have different different dependencies, as listed in
the table below. If there are no dependencies listed any UNIX-like system
should be adequate.

========= ================ ====================================================
Target    Dependencies     Description
========= ================ ====================================================
library   C99              Dynamic library for use by other programs
binaries  C99              Command-line binaries
headers                    Header files for the library
manuals   mdoc             All manual pages
tests     Pytest_          Run tests
========= ================ ====================================================

The manual pages require an implementation of `roff` which can process macros
in the `mdoc` format to read them. Building the manual has no dependencies.

The following makefile variables are available for setting according to your
needs (in addition to the usual makefile variables):


Testing
#######

IPS-Tools uses functional tests for the executables; the modules which are
wrapped by the executables provide only one function, so there is not need to
unit-test those. To run tests run `meson test -C build`. The test framework is
Pytest_  for functional tests.


.. ----------------------------------------------------------------------------
.. _Meson: https://mesonbuild.com/
.. _Ninja: https://ninja-build.org/
.. _Pytest: https://docs.pytest.org/
