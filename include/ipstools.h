/* Copyright (C) 2016-2022  Alejandro Sanchez
 *
 * IPS-Tools is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * IPS-Tools is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * IPS-Tools.  If not, see <http://www.gnu.org/licenses/>.
 */

/** @file info.h
 *
 *  Public interface to the IPS-Tools library.
 */

#ifndef IPS_TOOLS_H
#define IPS_TOOLS_H

#include <stdio.h>
#include <stdint.h>

/** Return codes from the info-procedure. */
enum {
	/** Info has been successfully been printed. */
	IPS_INFO_SUCCESS,
	/** Files are invalid or unreadable. */
	IPS_INFO_INV_FILE,
	/** The header of the patch file is invalid. */
	IPS_INFO_INV_HEADER,
	/** A record of the patch file is invalid. */
	IPS_INFO_INV_RECORD,
};


/** Print information about the patch file to output file.
 *
 *  @param patch   File containing the IPS patch.
 *  @param output  File to write the info to.
 *
 *  @return  Error code, 0 on success.
 *
 *  @pre  The `patch` file must be readable and the `output` file must be
 *        writeable.
 *
 *  Note that this function will advance the file positions and potentially
 *  reach the EOF.
 */
int ips_info(FILE *restrict patch, FILE *restrict output);


/** Return codes from the diff-procedure. */
enum {
	/** Diff has been successfully been printed. */
	IPS_DIFF_SUCCESS,
	/** Files are invalid or unreadable. */
	IPS_DIFF_INV_FILE,
	/** Offset is to large to be written into an IPS record (3 bytes). */
	IPS_DIFF_OFFSET_OVERFLOW,
};


/** Write the patch generated from the difference between `original` and
 *  `modified` to `out`.
 *
 *  @param original  The original unmodified file.
 *  @param modified  The modified file.
 *  @param out       Output file to write the patch to.
 *
 *  @return  Error code, 0 on success.
 *
 *  @pre  The files must not be `NULL`, `original` and `modified` must be
 *        binary readable, `target` must be writable.
 *
 *  Note that this function will advance the file positions and potentially
 *  reach the EOF.
 */
int ips_diff(FILE *restrict original, FILE *restrict modified, FILE *restrict out);


/** Return codes from the patch-procedure. */
enum {
	/** Patch has been successfully applied. */
	IPS_PATCH_SUCCESS,
	/** Files are invalid or unreadable. */
	IPS_PATCH_INV_FILE,
	/** The contents of the patch file are not valid. */
	IPS_PATCH_INV_PATCH,
};

/** Apply the patch from a file to a binary file and write output to another
 *  binary.
 *
 *  @param source  File to apply the patch to.
 *  @param target  File to write the patched output to.
 *  @param patch   File to read the patch from.
 *  
 *  @return  Error code, 0 on success.
 *
 *  @pre  The files must not be `NULL`, `source` and `patch` must be readable,
 *        `target` must be writable.
 *       
 *  Note that this function will advance the file positions and potentially
 *  reach the EOF.
 */
int ips_patch(FILE *restrict source, FILE *restrict target, FILE *restrict patch);

/** All possible kinds of IPS records. */
enum ips_record_type {
	IPS_RECORD_PLAIN,       /**< Plain IPS record.      */
	IPS_RECORD_COMPRESSED,  /**< Compressed IPS record. */
	IPS_RECORD_EOF,         /**< End of the patch file. */
};

/** Tagged union describing all IPS records.
 *
 *  Use the `tag` to distinguish the type of record. If the tag has the value
 *  `IPS_RECORD_EOF`, then the record is not an actual record, but the marker
 *  for the end of the path; the remaining fields will have nonsense values.
 *  */
struct ips_record {
	enum ips_record_type tag;  /**< The tag for the type of record. */
	unsigned long offset;      /**< Offset into the source file.    */
	unsigned long length;      /**< Length of the data sequence.    */
	union {
		uint8_t *data;         /**< The data sequence.              */
		uint8_t datum;         /**< The byte to write repeatedly.   */
	};
};


/** Read a record from a patch file.
 *
 *  @param f       File pointer to the file to read from.
 *  @param record  Pointer to the record to store the result.
 *
 *  @return  On success `0`, non-`0` otherwise.
 *
 *  Read a record from the patch file `f`, store the result in the variable
 *  pointed to by `record`. The file position will be advanced even if an error
 *  occurs.
 *
 *  If the record is plain, memory for the `data` field will be allocated on
 *  the stack. The caller is responsible for this memory.
 */
int ips_read_record(FILE *f, struct ips_record *record);

/** Write a record to a patch file.
 *
 *  @param f       File pointer to the file to write to.
 *  @param record  Record to write to file.
 *
 *  @return  On success `0`, non-`0` otherwise.
 *
 *  Write the `record` to the patch file `f`. The file position will be
 *  advanced even if an error occurs.
 */
int ips_write_record(FILE *f, struct ips_record record);

/** Read the truncation information from a patch file.
 *
 *  @param f  File pointer to the file to read from.
 *  @param l  Pointer to where to store the result.
 *
 *  @return  On success `0`, non-`0` otherwise.
 */
int ips_read_truncate(FILE *f, unsigned long *l);

/** Write the truncation information to a patch file.
 *
 *  @param f  File pointer to the file to write to.
 *  @param l  Truncation to write to the file.
 *
 *  @return  On success `0`, non-`0` otherwise.
 */
int ips_write_truncate(FILE *f, unsigned long l);
#endif  /* IPS_TOOLS_H */
