# -------------------------------------------------------------------------
# Copyright (C) 2016-2019  Alejandro Sanchez
#
# IPS-Tools is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License, or (at your
# option) any later version.
#
# IPS-Tools is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
# more details.
#
# You should have received a copy of the GNU General Public License along
# with this program.  If not, see <http://www.gnu.org/licenses/>.
# -------------------------------------------------------------------------

# === Public variables ====================================================
BUILD  = ./build
PREFIX = /usr/local
CFLAGS = -std=c99 -Wall -Werror -g


# === Internal variables ==================================================
# Version number of the project; make sure that at build time this value is
# inserted everywhere where it matters.
version = 0.0.1

OBJ_DIR = $(BUILD)/obj
BIN_DIR = $(BUILD)/bin
INC_DIR = $(BUILD)/include
LIB_DIR = $(BUILD)/lib
MAN_DIR = $(BUILD)/share/man
TAR_DIR = $(BUILD)/tar
TST_DIR = $(BUILD)/test


# === Default targets =====================================================
.PHONY: all bin inc lib man tar install clean check help

## all      Build all binaries, headers, and documentation
all: bin inc lib man

## bin      Build binaries only
bin: $(subst ./src/cli,$(BIN_DIR),$(basename $(wildcard ./src/cli/*.c ./src/cli/*.sh)))

## inc      Build header files only
inc: $(subst ./src,$(INC_DIR),$(wildcard ./src/ips/*.h))

## lib      Build static library only
lib: $(LIB_DIR)/libipstools.a

## man      Build manual pages
man: $(patsubst ./man/%,$(MAN_DIR)/%,$(wildcard ./man/*/*.[1,3,5]))

## tar      Build tarball for distribution
tar: $(TAR_DIR)/ips-tools.tar.gz

## install  Install files on the system (might require sudo)
install:
	@mkdir -p $(PREFIX)
	@cd $(BUILD) && for d in bin include lib share; \
		do find $$d -type f -print \
		| xargs -I {} $(SHELL) -c \
		'cd .. && mkdir -p $(PREFIX)/`dirname {}` && cp $(BUILD)/{} $(PREFIX)/{}'; \
	done


## clean    Remove all build products
clean:
	@rm -rf $(BUILD)

## check    Run tests
check: functional-test unit-test

## help     Show this message
help:
	@echo "Usage:  make [<targets>]"
	@echo ""
	@sed -n 's/^##\s*/  /p' $<
	@echo ""


# === Binaries ============================================================
$(BIN_DIR)/%: ./src/cli/%.sh
	@mkdir -p $(@D)
	@sed 's/^IPS_VERSION=0.0.0$$/IPS_VERSION=$(version)/' <$< >$@
	@chmod +x $@

$(BIN_DIR)/%: src/cli/%.c lib inc
	@mkdir -p $(@D)
	@$(CC) -L$(LIB_DIR) -DIPS_VERSION='"$(version)"' -I$(INC_DIR) $(CFLAGS) -o $@ $< -lipstools


# === Objects =============================================================
$(OBJ_DIR)/%.o: $(addprefix ./src/ips/%., c h)
	@mkdir -p $(@D)
	@$(CC) $(CFLAGS) -c -o $@ $<


# === Libraries ===========================================================
$(LIB_DIR)/libipstools.a: $(patsubst ./src/ips/%.c,$(OBJ_DIR)/%.o,$(wildcard ./src/ips/*.c))
	@mkdir -p $(@D)
	@ar -rsc $@ $^


# === Headers =============================================================
$(INC_DIR)/ips/%.h: ./src/ips/%.h
	@mkdir -p $(@D)
	@cp $< $@


# === Manpages ============================================================
$(MAN_DIR)/%: ./man/%
	@mkdir -p $(@D)
	@cp $< $@


# === Testing =============================================================
.PHONY: functional-test unit-test

functional-test: bin $(wildcard test/functional/*.sh)
	@PREFIX=$(BUILD) PATH=$(BIN_DIR):$$PATH roundup test/functional/ips-*-test.sh

unit-test: $(patsubst ./test/unit/%.c,$(TST_DIR)/unit/bin/%,$(wildcard ./test/unit/*.c))
	@mkdir -p $(TST_DIR)/unit/patches
	@for TEST in $(TST_DIR)/unit/bin/*; \
		do $$TEST "test/unit/patches" "$(TST_DIR)/unit/patches"; \
		done

$(TST_DIR)/unit/bin/%: test/unit/%.c lib inc
	@mkdir -p $(@D)
	@$(CC) -I$(INC_DIR) -L$(LIB_DIR) -o $@ $< -lipstools -lcheck 


# === Tarballs ============================================================
$(TAR_DIR)/ips-tools.tar.gz: .
	@mkdir -p $(@D)
	@# Exclude the build directory and all hidden files and directories.
	@# '.[^/]*' is necessary or else the current directory will be excluded as
	@# well
	@tar -zc --exclude=$(BUILD) --exclude='.[^/]*' -f $@ $<
