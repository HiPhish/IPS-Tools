.. default-role:: code

#################
Testing IPS-Tools
#################


There are two  types of test:  unit test and functional tests.  Unit tests test
the individual units of the library, whereas functional tests test the binaries
for how tell they operate on files.
