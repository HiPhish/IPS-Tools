import os
from pathlib import Path
from subprocess import run


ips = os.getenv('IPS_BINARY', '')
binary = 'binary'
original = Path(__file__).parent / binary / 'identity.bin'


def test_patch_apply(patch: Path, modified: Path) -> None:
    """IPS Patch functional tests"""

    # Apply a patch to the original file and compare the result to a manually
    # patched file that is known to be correct.

    with open(original, 'rb') as infile:
        patch_call = run([ips, 'patch', patch], stdin=infile, capture_output=True)
    given = patch_call.stdout

    with open(modified, 'rb') as infile:
        expected = infile.read()

    assert given == expected
