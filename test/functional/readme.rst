.. default-role:: code

###############################
Functional Testing of IPS-Tools
###############################

Each test case takes a certain fixed *input* consisting of one or more files to
generate a *given* file which is then compared to an *expected* file. The
expected file is written by hand and must be guaranteed to be the correct for
the given input. If the given and expected do not differ the test has passed
successfully.

Furthermore, we also have to check the return value of the tool. If the tool
exits with non-zero even though we expected it to succeed the test fails and
there is no point in comparing values. Conversely, if the tool should fail but
exits with zero the test fails as well.

.. note::

   All binary files have the extension `.bin`, but they are not BIN archives,
   they are just some bags of bytes that just so happen to spell out some text.
   I used `.bin` simply to mean "binary".

Generally for every test we have the following test cases:

- Identity patch that does not modify anything.
- Plain record
- Compressed record
- Truncating patch
- Patch that increases the size of the file

This does not necessarily apply to all tests though. Patches and modified files
are shared between tests for simplicity.
