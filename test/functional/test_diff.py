import pytest
import os
from pathlib import Path
from subprocess import run


ips = os.getenv('IPS_BINARY', '')
original = Path(__file__).parent / 'binary' / 'identity.bin'


def get_diff(original: Path, modified: Path) -> bytes:
    diff_call = run([ips, 'diff', original, modified], capture_output=True)
    diff = diff_call.stdout
    return diff


@pytest.fixture(params=['extended', 'plain', 'truncated'])
def name(request) -> str:
    """Provides the name of a patch case."""
    return request.param


def test_patch_diff(tmp_path: Path, modified: Path) -> None:
    """IPS Diff functional tests"""

    # Generate a patch from the original and a modified file, then compare the
    # generated patch to a manually written patch.

    patch = tmp_path / 'patch.ips'
    diff = get_diff(original, modified)
    with open(patch, 'wb') as outfile:
        outfile.write(diff)

    with open(original, 'rb') as infile:
        patch_call = run([ips, 'patch', patch], stdin=infile, capture_output=True)

    given = patch_call.stdout
    with open(modified, 'rb') as infile:
        expected = infile.read()

    assert given == expected
