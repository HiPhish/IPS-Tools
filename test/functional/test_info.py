import os
from pathlib import Path
from subprocess import run


ips = os.getenv('IPS_BINARY', '')


# Generate an info file for a patch and compare it to a manually written info
# file.
def test_patch_info(patch: Path, expected: str) -> None:
    """IPS Info functional tests"""

    with open(patch, 'rb') as infile:
        completed_process = run([ips, 'info'], stdin=infile, capture_output=True)
    given = completed_process.stdout.decode('utf-8')

    assert given == expected
