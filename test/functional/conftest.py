import pytest
from pathlib import Path

names = ['compressed', 'extended', 'identity', 'plain', 'truncated']
patches_dir = Path(__file__).parent / 'patch'
info_dir = Path(__file__).parent / 'info'


@pytest.fixture(params=names)
def name(request) -> str:
    """Provides the name of a patch case."""
    return request.param


@pytest.fixture()
def patch_name(name: str) -> str:
    """Provides the name of a patch file"""
    return name + '.ips'


@pytest.fixture
def patch(patch_name: str) -> Path:
    """Provides the full path to a patch file"""
    return patches_dir / patch_name


@pytest.fixture
def modified(name: str) -> Path:
    """Provides the name of the modified binary file"""
    return Path(__file__).parent / 'binary' / (name + '.bin')


@pytest.fixture
def expected(name: str) -> str:
    """Provides the expected info text output"""
    infopath = info_dir / (name + '.txt')
    with open(infopath, 'r') as infile:
        return infile.read()
