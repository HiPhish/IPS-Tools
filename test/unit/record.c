/* Copyright (C) 2016-2018  Alejandro Sanchez
 *
 * IPS-Tools is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * IPS-Tools is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * IPS-Tools.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <check.h>
#include <ips/ips.h>
#include <ips/record.h>

FILE *patch = NULL;  /* The patch file to read from */

START_TEST(plain)
{
	fseek(patch, 5, SEEK_SET);

	struct ips_record record;
	uint8_t data[] = {0x0A, 0x0B, 0x0C, 0x0D};
	ips_read_record(patch, &record);
	ck_assert_int_eq(IPS_RECORD_PLAIN, record.tag);
	ck_assert_int_eq(3, record.offset);
	ck_assert_int_eq(4, record.length);
	for (int i = 0; i < record.length; ++i) {
		ck_assert_int_eq(data[i], record.data[i]);
	}
}
END_TEST

START_TEST(compressed)
{
	fseek(patch, 14, SEEK_SET);

	struct ips_record record;
	ips_read_record(patch, &record);
	ck_assert_int_eq(IPS_RECORD_COMPRESSED, record.tag);
	ck_assert_int_eq(1, record.offset);
	ck_assert_int_eq(7, record.length);
	ck_assert_int_eq(0x0F, record.datum);
}
END_TEST

START_TEST(eof)
{
	fseek(patch, 22, SEEK_SET);
	struct ips_record record;
	ips_read_record(patch, &record);
	ck_assert_int_eq(IPS_RECORD_EOF, record.tag);
}
END_TEST

Suite *record_suite(void) {
	Suite *s;
	TCase *tc_core;

	s = suite_create("Record");
	/* Core test case */
	tc_core = tcase_create("Core");

	tcase_add_test(tc_core, plain);
	tcase_add_test(tc_core, compressed);
	tcase_add_test(tc_core, eof);
	suite_add_tcase(s, tc_core);

	return s;
}

int main(int argc, char *argv[]) {
	if (argc != 3) {
		fprintf(stderr, "Test must be called with two directory paths as arguments.\n");
		return -1;
	} else {
		char *pfile = "patch.ips";
		char fname[strlen(argv[1]) + strlen(pfile) + 1];
		snprintf(fname, sizeof fname + 1, "%s/%s", argv[1], pfile);
		if (!(patch = fopen(fname, "rb"))) {
			fprintf(stderr, "Could not open patch file %s\n", fname);
			return -1;
		}
	}

	int number_failed;
	Suite *s;
	SRunner *sr;

	s = record_suite();
	sr = srunner_create(s);

	srunner_run_all(sr, CK_NORMAL);
	number_failed = srunner_ntests_failed(sr);
	srunner_free(sr);
	fclose(patch);
	return (number_failed == 0) ? 0 : number_failed;
}
