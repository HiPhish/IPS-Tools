/* Copyright (C) 2016-2018  Alejandro Sanchez
 *
 * IPS-Tools is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * IPS-Tools is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * IPS-Tools.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <check.h>
#include <ips/record.h>
#include <stdio.h>
#include <stdint.h>

FILE *patch = NULL;
Suite *record_suite(void);

/* Number of bytes written to disc, computed from a const array */
#define RECORD_LENGTH (sizeof bytes * sizeof *bytes)

START_TEST(plain)
{
	uint8_t bytes[] = {0x00, 0x00, 0x03, 0x00, 0x05, 0x0A, 0x0B, 0x0C, 0x0D, 0x0E};
	uint8_t written[sizeof bytes];
	struct ips_record record = {
		.tag = IPS_RECORD_PLAIN,
		.offset = 0x000003,
		.length = 0x0005,
		.data = bytes + 5,
	};
	if (ips_write_record(patch, record)) {
		ck_assert_msg(0, "Could not write patch");
	} else if (fseek(patch, -RECORD_LENGTH, SEEK_CUR)) {
		ck_assert_msg(0, "Could not rewind patch");
	} else if (fread(written, sizeof *written, sizeof written, patch) != sizeof written) {
		ck_assert_msg(0, "Could not read patch");
	} else if (memcmp(written, bytes, sizeof written * sizeof *written)) {
		ck_assert_msg(0, "Bytes written and read differ");
	}
}
END_TEST

START_TEST(compressed)
{
	uint8_t bytes[] = {0x00, 0x00, 0x03, 0x00, 0x00, 0x00, 0x05, 0x0F};
	uint8_t written[sizeof bytes];
	struct ips_record record = {
		.tag = IPS_RECORD_COMPRESSED,
		.offset = 0x000003,
		.length = 0x0005,
		.datum = bytes[7],
	};
	if (ips_write_record(patch, record)) {
		ck_assert_msg(0, "Could not write patch");
	} else if (fseek(patch, -RECORD_LENGTH, SEEK_CUR)) {
		ck_assert_msg(0, "Could not rewind patch");
	} else if (fread(written, sizeof *written, sizeof written, patch) != sizeof written) {
		ck_assert_msg(0, "Could not read patch");
	} else if (memcmp(written, bytes, sizeof written * sizeof *written)) {
		ck_assert_msg(0, "Bytes written and read differ");
	}
}
END_TEST

START_TEST(eof)
{
	uint8_t bytes[] = {'E', 'O', 'F'};
	uint8_t written[sizeof bytes];
	struct ips_record record = {.tag = IPS_RECORD_EOF};
	if (ips_write_record(patch, record)) {
		ck_assert_msg(0, "Could not write patch");
	} else if (fseek(patch, -RECORD_LENGTH, SEEK_CUR)) {
		ck_assert_msg(0, "Could not rewind patch");
	} else if (fread(written, sizeof *written, sizeof written, patch) != sizeof written) {
		ck_assert_msg(0, "Could not read patch");
	} else if (memcmp(written, bytes, sizeof written * sizeof *written)) {
		ck_assert_msg(0, "Bytes written and read differ");
	}
}
END_TEST

int main(int argc, char *argv[]) {
	if (argc != 3) {
		fprintf(stderr, "Test must be called with two directory paths as arguments.\n");
		return -1;
	} else {
		char *pfile = "patch.ips";
		char fname[strlen(argv[2]) + strlen(pfile) + 1];
		snprintf(fname, sizeof fname + 1, "%s/%s", argv[2], pfile);
		if (!(patch = fopen(fname, "wb+"))) {
			fprintf(stderr, "Could not open patch file %s\n", fname);
			return -1;
		}
	}
	
	Suite *s = record_suite();
	SRunner *sr = srunner_create(s);

	srunner_run_all(sr, CK_NORMAL);
	int number_failed = srunner_ntests_failed(sr);
	srunner_free(sr);
	fclose(patch);
	return (number_failed == 0) ? 0 : number_failed;
}

Suite *record_suite(void) {
	Suite *s;
	TCase *tc_core;

	s = suite_create("Record");
	/* Core test case */
	tc_core = tcase_create("Core");

	tcase_add_test(tc_core, plain);
	tcase_add_test(tc_core, compressed);
	tcase_add_test(tc_core, eof);
	suite_add_tcase(s, tc_core);

	return s;
}

