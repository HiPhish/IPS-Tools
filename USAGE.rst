############
 How to use
############

IPS-Tools is a set of command-line programs, if you are not familiar with the
command-line consider this a crash-course. If you are familiar with it see the
manpages for full information.

The main program is called `ips`, it is called with the first argument being
the name of the tool to use, followed by the arguments for that tool. For
instance, to patch a binary file called `source.bin` using a patchfile called
`patch.ips` and write the output to a file `target.bin` we would run

.. code-block:: sh

   ips patch patch.ips <source.bin >target.bin

It is also possible to call the patch program directly:

.. code-block:: sh

   ips-patch patch.ips <source.bin >target.bin

There is no difference effectively, `ips` is just a wrapper that passes on its
arguments. The `<` and `>` characters are used for redirection: `ips-patch`
reads from the standard input and writes to the standard output, which are by
default connected to the keyboard and scree respectively. Since we do not want
to type our binary by hand nor print it onto the screen we redirect standard
input and standard output to files instead. The patch is always read from a
file, that's why there is no redirection there. All files are given relative to
the current working directory.


ips-patch
#########

Synopsis:
   .. code-block:: man

      ips-patch <patchfile>

This program applies the patch read from `<patchfile>` to the binary read from
the standard input and writes the result to the standard output. If the patch
file is invalid the output will only be partially written and will be useless.
Check the return value of the program to know whether an error has occurred.


ips-info
########

Synopsis:
   .. code-block:: man

      ips-info <patchfile>

Print information about an IPS patch file. The first line is the header of the
patch, expected to be `PATCH` and every successive line is a record. A record
consists of the offset followed by the number of bytes and whether it is
compressed or not.

As an extension to the IPS format some files also have a truncation entry
following the records. This will only be displayed if the file actually
contains this extension.


ips-diff
########

Synopsis:
   .. code-block:: man

      ips-diff <original> <modified>

The diff tool compares the *original* and *modified* file and writes a patch to
the standard output. The current state of the diff tool is crude, it will
produce 100% correct patches but the patches will not have optimum size.
Creating an optimal patch is a non-trivial task and requires a solid
understanding of the mathematics behind IPS records. I have written the current
implementation because I consider a correct but imperfect diff tool to be
better than no diff tool at all.



libipstools
###########

This is the C-library form of IPS-Tools. In fact, the shell-programs above are
really just small shell wrappers around the library. You can use libipstools if
you want to employ IPS-Tools inside a larger program, such as a GUI
application.


The GUI client
##############

There is no GUI included with IPS-Tools, but there is an official one in the
making called IPS-Studio. Here is a mockup in the meantime.

.. code-block::

   ╒═╤═╤═╤═══════════════════════════════════════════════════╕
   │x│-│+│                 IPS-Studio                        │
   ╞═╧═╧═╧═══════════════════════════════════════════════════╡
   │    ╭──────────────┬────────────────┬───────────────╮    │
   │    │ Apply Patch  │ Generate Patch │ Inspect Patch │    │
   │    ╰──────────────┴────────────────┴───────────────╯    │
   │                                                         │
   │    ╭────────────╮                     ╭────────────╮    │
   │    │            │                     │            │    │
   │    │            │                     │            │    │
   │    │  Original  │                     │  Modified  │    │
   │    │            │                     │            │    │
   │    │            │                     │            │    │
   │    ╰────────────╯                     ╰────────────╯    │
   │                                                         │
   │                     ╭────────────╮                      │
   │                     │            │                      │
   │                     │            │                      │
   │                     │   Patch    │                      │
   │                     │            │                      │
   │                     │            │                      │
   │                     ╰────────────╯                      │
   │                                                         │
   └─────────────────────────────────────────────────────────┘

The user will drop files into the wells and depending on what files have been
dropped in different buttons will be enabled. After a file has been generated
it will appear inside the corresponding well and the user will be able to drag
& drop it to where they want it.
